@file:Suppress("UnstableApiUsage")

val catalogs = extensions.getByType<VersionCatalogsExtension>()

pluginManager.apply {
    // These statements use the version catalog to get the proper plugin and version for buildSrc.
    // These statements DO NOT configure dependencies for these plugins.  They must be defined
    // in the dependencies section, below.
    val libs = catalogs.named("libs")

    this.withPlugin("org.jetbrains.kotlin.plugin.allopen") {
        dependencies.addProvider("implementation", libs.findDependency("kotlin-allopen").get())
    }

    this.withPlugin("org.jetbrains.kotlin.jvm.gradle.plugin") {
        dependencies.addProvider("implementation", libs.findDependency("kotlin-jvm-gradle-plugin").get())
    }

    this.withPlugin("org.jetbrains.kotlin.kapt.gradle.plugin") {
        dependencies.addProvider("implementation", libs.findDependency("kotlin-kapt-gradle-plugin").get())
    }

    this.withPlugin("org.jetbrains.kotlin.noarg.gradle.plugin") {
        dependencies.addProvider("implementation", libs.findDependency("kotlin-noarg-gradle-plugin").get())
    }
}

plugins {
    `kotlin-dsl`
}

repositories {
    // Use the plugin portal to apply community plugins in convention plugins.
    gradlePluginPortal()
    mavenCentral()
}

dependencies {
    // Align versions of all Kotlin components
    //implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // These dependencies are used for the plugins in the convention.gradle.kts files.
    implementation(catalogs.named("libs").findDependency("kotlin-allopen").get())
    implementation(catalogs.named("libs").findDependency("kotlin-jvm-gradle-plugin").get())
    implementation(catalogs.named("libs").findDependency("kotlin-kapt-gradle-plugin").get())
    implementation(catalogs.named("libs").findDependency("kotlin-noarg-gradle-plugin").get())
    implementation(catalogs.named("libs").findDependency("kotlin-reflect").get())
    implementation(catalogs.named("libs").findDependency("kotlin-stdlib").get())
}