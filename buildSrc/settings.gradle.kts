@file:Suppress("UnstableApiUsage")

// Using the same version catalog for buildSrc as for the other projects.
dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            from(files("../gradle/libs.versions.toml"))
        }
    }
}
