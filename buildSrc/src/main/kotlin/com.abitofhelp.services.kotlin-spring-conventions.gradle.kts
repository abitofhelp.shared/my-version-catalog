plugins {
    // Apply the common convention plugin for shared build configuration between library and application projects.
    id("com.abitofhelp.services.kotlin-common-conventions")
    id( "org.jetbrains.kotlin.plugin.allopen")
}

val catalogs = extensions.getByType<VersionCatalogsExtension>()

noArg {
    annotation("org.springframework.data.mongodb.core.mapping.Document")
}

repositories {
    // Use the plugin portal to apply community plugins in convention plugins.
    gradlePluginPortal()
    mavenCentral()
}

dependencies {
    implementation(catalogs.named("libs").findDependency("kotlin-reflect").get())
    implementation(catalogs.named("libs").findDependency("kotlin-stdlib").get())

    // Use JUnit Jupiter for testing.
    testImplementation(catalogs.named("libs").findDependency("junit-jupiter").get())
}

tasks.test {
    // Use junit platform for unit tests.
    useJUnitPlatform()
}
