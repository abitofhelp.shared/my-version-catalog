@file:Suppress("UnstableApiUsage")

val catalogs = extensions.getByType<VersionCatalogsExtension>()

plugins {
    id( "org.jetbrains.kotlin.plugin.allopen")
    id("org.jetbrains.kotlin.jvm")
    id("org.jetbrains.kotlin.kapt")
    id("org.jetbrains.kotlin.plugin.noarg")
}

repositories {
    // Use the plugin portal to apply community plugins in convention plugins.
    gradlePluginPortal()
    mavenCentral()
}

dependencies {
    implementation(catalogs.named("libs").findDependency("kotlin-reflect").get())
    implementation(catalogs.named("libs").findDependency("kotlin-stdlib").get())

    // Use JUnit Jupiter for testing.
    testImplementation(catalogs.named("libs").findDependency("junit-jupiter").get())
    testImplementation(catalogs.named("libs").findDependency("assertj-core").get())
}

kapt {
    arguments {
        // Set Mapstruct Configuration options here
        // https://kotlinlang.org/docs/reference/kapt.html#annotation-processor-arguments
        // https://mapstruct.org/documentation/stable/reference/html/#configuration-options
        // arg("mapstruct.defaultComponentModel", "spring")
    }
}

tasks.test {
    // Use junit platform for unit tests.
    useJUnitPlatform()
}
