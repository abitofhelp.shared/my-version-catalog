enableFeaturePreview("VERSION_CATALOGS")

rootProject.name = "my-version-catalog"
include("app", "utilities")

// Centralized management of plugin versions.
// At this time, we cannot use the version
// catalog for plugins in our build scripts,
// nor here.
//pluginManagement {
//    plugins {
//        id("me.champeau.jmh") version("0.6.3")
//    }
//}