plugins {
    id("com.abitofhelp.services.kotlin-application-conventions")
}

dependencies {
    implementation(project(":utilities"))
    //implementation(platform(libs.arrow.stack))
    //implementation(libs.arrow.core)
}

application {
    // Define the main class for the application.
    mainClass.set("com.abitofhelp.services.app.AppKt")
}
